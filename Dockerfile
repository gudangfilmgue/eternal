FROM php:7.3.6-apache

WORKDIR /src

# Install Dependencies
RUN apt-get update && \
    apt-get install -y zlib1g-dev libfreetype6-dev libpng-dev libjpeg-dev libwebp-dev libzip-dev unzip && \
    apt-get clean && \
# Install extensions
    docker-php-ext-install pdo_mysql mysqli zip exif pcntl sockets && \
    docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp && \
    docker-php-ext-install gd

RUN apt install -y git && \
    apt install -y nano && \
    apt install -y curl && \
    apt install -y wget && \
    apt clean && \
    rm -rf /var/cache/apt/archives

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install mPDF
RUN composer require mpdf/mpdf

RUN echo "TZ=Asia/Jakarta" >> /etc/environment

RUN a2enmod rewrite

COPY conf/php.ini "$PHP_INI_DIR/"

COPY src /src
RUN chmod -R 775 /src

EXPOSE 80
EXPOSE 443
